(function (window, $) {
    
    "use strict";
    var $link = $("<meta charset='UTF-8'><meta name='viewport' content='width=device-width, initial-scale=1.0'><link type='text/css' rel='stylesheet' href='//localhost/fnac/css/xbox_small.css'>").appendTo("head");
    var htmlString = $("<div><div class='xbox1' id='fnacxbox1'><div class='container'><div class='pigi'><a id='bouton_fnacxbox2' href='http://jeux-video.fnac.com/Console-Microsoft-Xbox-One-Edition-Limitee-1-To-Halo-5-Guardians/a8942591/w-4'><img src='//localhost/fnac/img/pegi16_03.jpg'/></a></div><div class='logo-halo'><img src='//localhost/fnac/img/logo_halo_03.png'/></div><div class='track'><img class='claim' src='//localhost/fnac/img/claim_02.png'/></div><div class='button'><img src='//localhost/fnac/img/CTA_03.png'/></div></div><div class='banner'><div class='exclu'><img class='exclu1' src='//localhost/fnac/img/exclu_small.png'/></div></div></div><img class='ferme1' id='close1' src='//localhost/fnac/img/btn_fermer.png'></div><div><div class='xbox2' id='fnacxbox2'><div class='container2' id='containers'><div class='head'><img class='h1-1' src='//localhost/fnac/img/perso_gauche_01.png'/><img class='h1-2' src='//localhost/fnac/img/perso_gauche.png'/><div class='pigi'><img src='//localhost/fnac/img/pegi16_03.jpg'/></div></div><div class='logo-halo'><img src='//localhost/fnac/img/logo_halo_03.png'/></div><div class='button'><img src='//localhost/fnac/img/CTA_03.png'/></div><div class='head2'><img class='h2-1' src='//localhost/fnac/img/perso_droite_02.png'/><img class='h2-2' src='//localhost/fnac/img/perso_droite.png'/></div></div><div class='banner'><div class='exclu'><img class='exclu1' src='//localhost/fnac/img/exclu_small.png'/><img class='exclu2' src='//localhost/fnac/img/exclu_small2.png'/></div></div></div><img class='ferme' id='close2' src='//localhost/fnac/img/btn_fermer.png'></div>").appendTo("body");
    
    
     $('#close1').click(function() {
        $("#close1").hide();
        $('#fnacxbox1').filter(':animated').promise().done(function() { 
            $("#fnacxbox1").animate({
                bottom: "-60px",
            }, 1200);
            
            $("#fnacxbox2").animate({
                bottom: "-150px",
            }, 1200);
            $("#close2").animate({
                bottom: "-150px",
            }, 1200);
        });
     });
     
     $('#close2').click(function() {
        $("#fnacxbox1").animate({
            bottom: "-60px",
        }, 1200);
        $("#close1").animate({
            bottom: "-60px",
        }, 1200);
        $("#fnacxbox2").animate({
            bottom: "-150px",
        }, 1200);
        $("#close2").animate({
            bottom: "-150px",
        }, 1200);
     });
    
     $('#fnacxbox1').click(function() {
         console.log($('#fnacxbox1').css('bottom'));
        console.log($('#fnacxbox2').css('bottom'));
        
        $("#fnacxbox1").animate({
            bottom: "-60px",
        }, 1200);
        $("#close1").hide();
        $('#fnacxbox1').filter(':animated').promise().done(function() {
            $("#fnacxbox2").animate({
                bottom: "0px",
            }, 1200);
            $('#fnacxbox2').filter(':animated').promise().done(function() {
                $("#close2").show();
            });
        });
        
        
     });
     
     $('#fnacxbox2').click(function() {
         
         console.log($('#fnacxbox1').css('bottom'));
         console.log($('#fnacxbox2').css('bottom'));
        
        $("#fnacxbox2").animate({
            bottom: "-150px",
        }, 1200); 
        $("#close2").hide(); 
        $('#fnacxbox2').filter(':animated').promise().done(function() { 
            $("#fnacxbox1").animate({
                bottom: "0px",
            }, 1200);
            $('#fnacxbox1').filter(':animated').promise().done(function() {
                $("#close1").show();
            });
        });
        
     });
     
     $(window).resize(function(){
                var w = $(window).width();
                
                if(w < 720) {
                    console.log(w);
                    $('#banner').display = 'none';
                } else {
                    $('body').show();
                }
            });
} (this, this.jQuery));
